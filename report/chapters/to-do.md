Sunday 24:
Chapter 1:
-LR ABM
-LR Paramters/UI - Disaster Sim
-LR Supermarket models

How do I want to Structure Chapter 1

We will demonstrate the effectiveness of this type of model, by displaying a visual animation as an output which can be interfaced with a Graphical User Interface (GUI) in order to take live visual and data output from the model in real-time. 
In order to demonstrate the benefits of picking this method as way of visual demonstration is that for a model to provide accurate statistical analysis it comes at a cost of being highly overfitted to the specific area, demographic, and not being able to interact 
with the simulation in real-time. [] 


Then insert details on visual simulations:
 By using computational systems we can emulate real life processes, and play out scenarios that allow us to reach conclusions and see outcomes when provided
with a controlled set of inputs. 


-History of simulations
-

Begin Lit Review:
Critical Analysis of Existing solutions
Here we look at a 



-OpenABM
-Washington Post
-


Notes: Outputs from a model to suggest how (non-pharmsuitical interventions) NPIs can be implemented 



Disease transmission method will be airborne.
Therefore, this report outlines a model with the key feature of having the mode of COVID infection transmission from agent to agent being 
a cough/sneeze. This extends outwards from the agent, if another agent is caught in the trajectory of that projection then there is the possibility of infection.
The method documented in this report combines the idea of asymptomatic and symptomatic person infection.



Chapter 2: Random spread of agents thorughout the environemnt is justified due to the chaotic nature of the COVID virus itself. 



Ch2:


\begin{verbbox}[\small]
    xOff, yOff = 0
    for(i=0 -> radius, i++)
        xOff++
        bounce = noise(xOff, yOff) 
        mappedBounce = map(bounce, 0, 1, -1, 1)
        vertex(i, mappedBounce)

    for(i=0 -> radius, i++)
        yOff++
        bounce = noise(xOff, yOff) 
        mappedBounce = map(bounce, 0, 1, -1, 1)
        vertex(radius+mappedBounce, i)

    for(i=0 -> radius, i++)
        xOff--
        bounce = noise(xOff, yOff) 
        mappedBounce = map(bounce, 0, 1, -1, 1)
        vertex(i, radius+mappedBounce)

    for(i=0 -> radius, i++)
        yOff--
        bounce = noise(xOff, yOff) 
        mappedBounce = map(bounce, 0, 1, -1, 1)
        vertex(mappedBounce, i)
    
\end{verbbox}
    
\begin{figure}[h!]
    \centering
    \theverbbox\qquad
    \includegraphics[scale=0.5]{perlin-drawing.jpg}
    \caption{Drawing sides of a square with height variation}
    
\end{figure}