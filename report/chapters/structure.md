Summary

Chapter 1 Structure:
-Introduction DONE
-Background Research
-Lit Review
    -Simulations
        -History of simulation
        -Successful Simulation Conditions
    -Critical Analysis of Existing solutions
        -
    -ABM
        -Agent Behaviours
    -Visually approx disease transmissions

-Aims
-Objectives


Chapter 2 Structure:
-ABM
    -Implementation technologies
    -Classes
    -AA
    -Behaviours
        -Seeking
        -Evading
-Visualising Disease Transmission
    -Shape
    -Firing
-Visualising Social Distance 
-GUI
    -Parameter Controls
-Storing Model Data - Data Structures
-Testing Methods
    -Survey Questions
    -Automatic Data Collection
-Data Visualisation
-Code Structure and Organisation
    -Version Control
    -Sprint Methodology


Chapter 3 Structure:
-Simulation Visual Outputs
-Model flaws
    -Coughing fits
    -Infection Shape
-User Experience
    -Internal Review
    -External Review


Chapter 4 Structure:
-Demonstrating Model Success
    -Versatility
    -Adaptablility
        -Scalability
    -Functionality
    -Usability
-Validation
-Conclusion

-Ideas for future work







