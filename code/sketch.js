var numAgents = 3;
var numItems = 10;
var numInfected = 1;
var numItemsToPickUp = 1;
var timeOffset = 0;
let table;
let sel;
var parameterToTest;
var simWidth = 800;
var simHeight = 800;

function preload() {
	//tableAgents = new p5.Table();
	//tableSocial = new p5.Table();
	//tableItems = new p5.Table();
}


function setup()
{
	// agentInfectionTimings = [];
	// agentPoints = [];
	// socialInfectionTimings = [];
	// itemInfectionTimings = [];

	// for (var i=0; i<12; i++)
	// 	tableAgents.addColumn(i);
	// print(tableAgents.getColumnCount());

	createCanvas(1600,800);
	frameRate(30);
	background(153);

	plot = new GPlot(this);
	plot.setPos(850, 50);
	socialDistancePlot = new GPlot(this);
	socialDistancePlot.setPos(850, 400);
	//plot.setPoints(points);

	colorNames = ['maroon', 'red', 'purple', 'fuchsia', 'green', 'lime', 'olive', 'yellow', 'navy', 'blue', 'teal', 'aqua'];
	//var randomColor = Math.round((random(0,12)));
	//print(colorNames[randomColor]);
	textNumberAgents = createP('Number of Agents (1-999)');
	textBoxNumberAgents = createInput('Enter Number, Default: 3');
	var textBoxNumber;
	textBoxNumberAgents.input(textBoxNumberAgentsInputEvent);

	textInfected = createP('Number of Infected Agents (1-12)');
	textBoxInfected = createInput('Enter Number, Default: 1');
	//var textBoxInfected;
	textBoxInfected.input(textBoxInfectedInputEvent);

	//<p> element creates a paragraph
	textBounceFactor = createP('Cough Shape Randomness');
	sliderBounceFactor = createSlider(1, 150, 1, 1);

	textCoughSize = createP('Cough Size');
	sliderCoughSize = createSlider(1, 150, 100, 1);

	textCoughSize = createP('Cough Frequency');
	sliderCoughWobbleFactor = createSlider(0.01, 1, 0.1, 0);

	socialDistanceDefaultValue = 100;
	socialDistanceCheckbox = createCheckbox('Enable Social Distancing', true);
	socialDistanceCheckbox.changed(socialDistanceCheckedEvent);
	socialDistanceMode = true;
	textSocialDistance = createP('Social Distance');
	sliderSocialDistance = createSlider(0, 500, socialDistanceDefaultValue, 0);

	//option for agents to leave store after picking up certain amount of items
	leaveStoreCheckbox = createCheckbox('Remove agents from environment after they have completed task', false);
	leaveStoreCheckbox.changed(leaveStoreCheckedEvent);
	leaveStoreMode = false;
	sliderLeaveStore = createSlider(3,10,5,1);

	textAutoTest = createP('Auto-test');
	parameterSelect = createSelect();
	parameterSelect.option('Agents');
	parameterSelect.option('Distance');
	parameterSelect.option('Items');
	//parameterSelect.selected(agent);


	

	//textClock = createP(second());

	ready = false;
	reset = false;
	iterationTimes = [];
	iterations = 1; //we start on the first iteration to make indexing easier

	infectionTimings = []

	//Start Simulation button
	readyButton = createButton('Start Simulation');
	readyButton.mousePressed(function(){
		ready=true;
		timeOffset = int(millis()/1000);
		print(timeOffset);
	});

	//These global variables will be accessible to all agent objects
	//It is not a good practice to allow agents to access other agents functions/members
	//however for quick prototyping we'll use it for now, may refactor if time permits
	agents = [];
	items = [];
	itemCoordinates = [];
	//agentCoordinates = [];

	
	


	

	noLoop();
	
}

//adds items and agents to their arrays in order to draw them
function initialise() {
	currentTime = 0;
	agents = [];
	items = [];
	itemCoordinates = [];
	numItemsToPickUp = sliderLeaveStore.value();
	//items.push(new Item("cat food", "blue"));
	//items.push(new Item("beef", "red"));

	for (var i=0; i < numItems; i++) {
	items.push(new Item("cat food", random(colorNames)));
		itemCoordinates.push(createVector(items[i].position.x,items[i].position.y));
	}

	//spawn agents in centre
	for (i=0; i < numAgents; i++) {
		var randomColor = Math.round((random(0,12)));
		print(randomColor);
		agents.push(new Agent(random(0,simWidth), random(0,simHeight),"BILL", random(colorNames)));
		//agentCoordinates.push(createVector(agents[i].position.x, agents[i].position.y));
	}

	//agents.push(new Agent(random(0,width), random(0,height), "BILL", "yellow"));
	//agents.push(new Agent(random(0,width), random(0,height), "FREDDY", "brown"));
	//agents.push(new Agent(random(0,width), random(0,height), "EREN JAEGAR", "pink"));
	for (var i = 0; i < numInfected; i++)
		agents[i].infected = true;

	reset = true;
}


function draw() 
{
	line(800,0,800,800);

	if (ready) {
		if (!reset)
		{
			initialise();
		}




		// print(frameRate());
		background(153);
		line(800,0,800,800);
		
		angleMode(DEGREES);
		/* draw a square of random width, height, bouncy lines, height
		of bounce, location on screen */
		
		//draw in the centre of the screen
		//itemCoordinates[0].x = mouseX;
		//itemCoordinates[0].y = mouseY;

		
		for (var i=0; i<numAgents; i++) {
			agents[i].draw();
			agents[i].move();
			//agentCoordinates[i].x = agents[i].position.x;
			//agentCoordinates[i].y = agents[i].position.y;
			if(socialDistanceMode)
				agents[i].socialDistanceValue = sliderSocialDistance.value();
			else
				agents[i].socialDistanceValue = 1;

			//agent will seek the closest item 
			//print(agentCoordinates[i]);
			//agents[i].seek();
		}

		for (var i=0; i<numItems; i++) {
			items[i].draw();
		}


		circle(mouseX,mouseY,20);

		

		var totalRunTime = int(millis()/1000) - timeOffset;
		fill('black');
		text("Total Run Time: " + totalRunTime, 20, 280);

		text("Current Time: " + getTime(), 20, 300);
		// print(frameRate());

		//noLoop();
		
		drawGraph();

		if(checkEndState()) {
			print ("ALL INFECTED, STOP THE CLOCK, RESET");
			print("ITERATION: " + iterations + " TIME: " + getTime());
			iterationTimes.push(totalRunTime);
			drawGraph();
			//saveCanvas(iterations.toString(), 'jpg');
			print("iteration times: " + iterationTimes);
			print("infection times: " + infectionTimings);
			// updateTables();
			iterations++;
			reset = false;
			infectionTimings = [];

			//if auto testing on
			//ready = false;
		}

	}
}

//we will update the array with starting length 1 with the 
function getTime() {
	//if it's the first iteration return the sketch active time, the time array will be empty
	if(iterations == 1)
		return int(millis()/1000)-timeOffset;
	//millis() returns the number of milliseconds since the sketch has launched
	//we want separate time per iteration, the sketch doesn't reload through iterations 
	//iterations EQUALS iterationTimes.length
	else {
		var currentIterationTime = int(millis()/1000) - iterationTimes[iterationTimes.length-1];
		return currentIterationTime-timeOffset;
	}
}

function mousePressed() {
	loop();
	//agents[0].printCoordinates();
	//print(agents);
}
function mouseReleased() {
	//noLoop();
}

//For testing purposes the enter key will infect all the agents, effectively resetting the simulation and starting the next iteration
function keyPressed() {
	if (keyCode === ENTER) {
		for (var i = 0; i < numAgents; i++)
			agents[i].infected = true;
	}
}

function socialDistanceCheckedEvent() {
	if (socialDistanceCheckbox.checked()) {
		console.log('social distance enabled');
		socialDistanceMode = true;
	} 
	else {
		console.log('social distance disabled');
		socialDistanceMode = false;
	}

}

function leaveStoreCheckedEvent() {
	if (leaveStoreCheckbox.checked())
		leaveStoreMode = true;
	else 
		leaveStoreMode = false;
}

function startSimulation() {
	//before starting the sim the sketch will track time, so need to reduce all times by this pre start value
	timeOffset = int(millis()/1000);
	print("set up time = " + timeOffset);
	ready = true;
}


function checkEndState() {
	var tally = 0;
	for (var i = 0; i < numAgents; i++) {
		if (agents[i].infected) {
			tally++;
		}
	}

	//if all agents are infected
	if (tally == numAgents) {
		//noLoop();
		print(parameterSelect.value());
		nextIteration();
		
		return true;
	}
}

// function updateTables() {
// 	//newAgentTimings is a reference to the new row in the table, so we add data to the reference
// 	// tableAgents.addRow();
// 	// for (var i = 0; i < infectionTimings.length; i++) {
// 	// 	tableAgents.setNum(tableAgents.getRowCount()-1, i, infectionTimings[i]);
// 	// }
// }

function textBoxNumberAgentsInputEvent() {
	numAgents = this.value();
}

function textBoxInfectedInputEvent() {
	numInfected = this.value();
}

function nextIteration() {
	//var mode;
	switch (parameterSelect.value()) {
		case 'Agents':
			if(numAgents < 1000) {
				numAgents++;
				print("continuing with agents, adding another");
			}
			else {
				print("finished collecting data on Number Agents, stopping simulation");
				ready = false;
			}
			break;
		case 'Distance':
			//socialDistanceValue += 5;
			break;
		
		case 'Items':
			numItemsToPickUp++;
			break;
		default:
			numAgents++;
			break;
	} 
}

//i have two directions to take, 1 graph is done, can do avg 
var plotIterationTimes = [];
const drawGraph = () => {
	var agentPoints = [];
	for (var i = 0; i < infectionTimings.length; i++) {
		agentPoints[i] = new GPoint(infectionTimings[i], i);

	}
	for (var i = 0; i < iterationTimes.length; i++) {
		if (iterationTimes.length==1)
			plotIterationTimes[0] = new GPoint(0, iterationTimes[0]);
		else {
			plotIterationTimes[0] = new GPoint(0, iterationTimes[0]);
			plotIterationTimes[i] = new GPoint(i, iterationTimes[i]-iterationTimes[i-1]);

		}
	}
	//print(infectionTimings);
	plot.setLineColor(this.color(0, 255, 255));
	plot.setPoints(agentPoints);
	plot.setTitleText("Tracking each agent's infection time");
	plot.getYAxis().setAxisLabelText("Number of agents infected");

	plot.getXAxis().setAxisLabelText("Time of Infection");
	plot.defaultDraw();

	// var agentPoints = [];
	// var distancePoints = [];
	// for (var i = 0; i < infectionTimings.length; i++) {
	// 	agentPoints[i] = new GPoint(infectionTimings[i], i);
	// }
	//print(infectionTimings);
	socialDistancePlot.setLineColor(this.color(255, 0, 0));
	socialDistancePlot.setPoints(plotIterationTimes);
	socialDistancePlot.setTitleText("Time for each iteration");
	socialDistancePlot.getXAxis().setAxisLabelText("Iteration number");
	socialDistancePlot.getYAxis().setAxisLabelText("Time to complete iteration");
	socialDistancePlot.defaultDraw();

}


/*


IDEAS AND TODO
as time goes on, infected agents will become more contagious, their squiggles will become larger in radius, up to a certain point
speed up time, slow down time, stop
Have the option for social distancing 
Then see the difference in sequence from not social distancing
Cough option, rapid expansion of patient contagion zone periodically, possible tiers for patients to show how infections they are
Mask option, extereme reduction in contagion zone when patient wears mask


MVP Requirements
1) Agents will seek randomly located supermarket items DONE
2) User will be able to select the number of infected DONE
3) Infected agents will cough periodically, any agent within infection zone
will be infected and then spread the illness DONE


 next make these parameters public, accessible 
	FIRST through the use of sliders 

	Next combine the interface so we have a navigatable plane?? 
	 
DISS REQUIREMENTS - END CONDITION, RESULTS TESTING 
We want a useful output from this simulation, so we will graph "time taken to infect everyone" against "variance in parameter"
IF there is a high gradient then that parameter is essential in controlling the simulation,
IF NOT then it's only cosmetic

FURTHER: We will ask the users if the deemed 'not useful' parameters were helpful or nice to look at

Potential problems - if fast forward isn't implemented then we will have to test in real-time (for multiple tests (eg 10) for same parameter value this
will be long)


Known bugs:
1) Social Distance Circle Marker will not update size when dragging the slider unless the checkbox is double tapped
2) Coughing is represented as a square but covers a circular distance, so agents in the cicrle will get infected even though
	graphically they have not touched the square cough zone
3) Performance drops
4) No textbox validation
const nextIteration = () => {	
 */