class Agent {
    constructor(x, y, name, colour) {
        
        this.agentSize = 20;
        this.mass = 1;
        this.position = createVector(x, y);
        this.velocity = p5.Vector.random2D();
        this.acceleration = createVector(0, 0);
        //since force is self applied, walking, it is limited ie thrust
        this.max_force = 1;
        //once getting up to speed (accelerating), agent will walk at constant top pace
        this.max_speed = 1;
        this.orientation = p5.Vector.random2D();
        // print(this.orientation);
        this.point2 = p5.Vector.random2D();

        this.targetCoordinates = createVector(350, 350);
        this.lerpStep = 0.01;
        this.headingFromTarget = 0;
        this.targetVector = createVector(0,0);
        this.steeringForce = createVector(0,0);

        // this.coughRate = 0;
        this.coughSize = 100;

        this.xoff = 0;
        this.yoff = 0;


        this.closestItemLocation = createVector(-999, -999);
        this.closestItem;
        this.agentMap = [];


        //infected parameters
        this.infected = false;
        this.tallied = false;
        this.coughed = false;
        this.coughingAnimation = false
        this.coughTravelSpeed = 5;
        this.coughFrequency;
        this.mask = false;
        this.socialDistanceValue = sliderSocialDistance.value();


        //flags for switching target item and evader
        this.evader = false;
        this.agentToEvade;
        this.itemMap = items.slice();
        this.iAmGoingToSwitch = false;
        this.itemsToIgnore;
        this.numItemsPickedUp = 0;
        this.finishedShopping = false;

        //debugging
        this.name = name;
        this.colour = colour;

        
    }

    //Will be called outside by agent manager when 
    updateItemMap() {
        this.itemMap = items.slice();
    }
    
    checkPickedUpItem() {
        //print(this.position);
        //print(this.closestItemLocation);
        if (this.position.dist(this.closestItemLocation) <= 10) {
            //print("I AM ON TOP OF TARGET");
            for (var i=0; i<items.length; i++) {
                if (items[i].position.equals(this.closestItemLocation)) {
                    items[i].respawn();
                }
            }
            this.numItemsPickedUp++;
        }
        if (leaveStoreMode && this.numItemsPickedUp >= numItemsToPickUp) {
            print("finished shopping");
            this.leaveStore();
        }
    }

    findClosestItem() {
        //to find closest item we will find the distance between the item
        //coordinates and the agent 
        //we will only have to do this once for performance 
        //print(this.name + " CAN CURRENTLY SEE " + this.itemMap.length + " ITEMS");
        //print(itemCoordinates);
        
        //if true then
        if (this.itemsToIgnore != null) {

            //print("finding new closest item that is not " + this.itemsToIgnore.name);
            for (var i=0; i<this.itemMap.length; i++) {
                //var hoopla = 0;
                //print(items[i].name);
                if (this.itemsToIgnore != this.itemMap[i]) {
                    this.closestItemLocation = this.itemMap[i].position; // temp
                    //print(this.itemsToIgnore.name + " is not " + this.itemMap[i].name);
                    if (this.position.dist(this.itemMap[i].position) <= this.position.dist(this.closestItemLocation)) {
                        this.closestItemLocation = this.itemMap[i].position;
                        this.closestItem = this.itemMap[i];
                        //print("my name is " + this.name + " and my new closest is " + this.closestItem.name); // this is never printing 
                    }
                }
            }
        }
        else {
            for (var i=0; i<this.itemMap.length; i++) {
                //var hoopla = 0;
                //print(items[i].name);
                
                if (this.position.dist(this.itemMap[i].position) < this.position.dist(this.closestItemLocation)) {
                    this.closestItemLocation = this.itemMap[i].position;
                    this.closestItem = this.itemMap[i];
                    //print("my name is " + this.name + " and my new closest is " + this.closestItem); // this is never printing 
                }
                
            }
        }
        //var index = this.itemMap.indexOf(this.closestItem);
        //print(this.itemMap[index]);
    }

    switchTarget() {
        if(this.iAmGoingToSwitch) {
            var index = this.itemMap.indexOf(this.closestItem);
            this.itemsToIgnore = this.itemMap[index];
            //print("I " + this.name + " am now ignoring " + this.itemsToIgnore.name);
            //print(this.itemsToIgnore);
            //this.itemMap.splice(index, 1); //THIS SEEMS TO BE AFFECTING THE ACTUAL ITEM LIST
            //print(this.itemMap.length);
            //this.findClosestItem();
            //print("I " + this.name + " am now searching for " + this.closestItem.name);
            for (var i=0; i < this.itemMap.length; i++) {
                if (this.itemMap[i].position == this.closestItemLocation) {
                    
                }
            }
        }
    }

    
    seek() {
        //print(agentCoordinates); //ARRAY
        //print("Seeking");
        //this.flee(agentCoordinates);
        //push the vehicle in direction such that its trajectory is pointing towards target
        this.targetCoordinates.set(this.closestItemLocation.x, this.closestItemLocation.y);
        
        var trajectory = p5.Vector.sub(this.targetCoordinates, this.position);
        trajectory.normalize(); //set's max speed to 1
        this.targetVector = trajectory;
        //rotate(targetVector.heading());
        // if(this.flee())
        // 	var x = 1;
        // else
        

        this.steeringForce = p5.Vector.sub(this.targetVector, this.velocity);

        //this.orientation = p5.Vector.lerp(this.orientation, targetVector, this.headingFromTarget);
        // var mag = p5.Vector.mag(this.orientation);
        //this.orientation = this.orientation.normalize();
        //this.headingFromTarget += this.lerpStep;
        //we can use the lerp function to rotate the orientation vector towards the target vector
        //constantly turn vehicle towards orientation 
        //constantly move in direction of orientation 
    }

    //there is a problem with using a global agent coordinate list,
    //in that an agent will have it's own distance in their when comparing
    // buildAgentMap() {

    // }


    checkBreakingSocialDistance() {
        for(var i=0; i < agents.length; i++) {
            //if the agents distance from another is less than the social distance limit then flee in the other direction
            if (this.position.equals(agents[i].position))
                continue;
            if (this.position.dist(agents[i].position) <= this.socialDistanceValue) {
                this.agentToEvade = agents[i];
                return true;
            }
        }
    }

    /*
    flee() {
        for(var i=0; i < agents.length; i++) {
            //if the agents distance from another is less than the social distance limit then flee in the other direction
            if (this.position.equals(agents[i].position))
                continue;
            if (this.position.dist(agents[i].position) <= this.socialDistanceValue) {
                var targetVector = this.evade(agents[i]);
                this.steeringForce = p5.Vector.sub(targetVector.mult(-1), this.velocity);
                return true;
            }
        }
    }
    */

    evade() {
        //let enemyFuturePosition = this.agentToEvade.position.copy();
        //enemyFuturePosition.add(this.agentToEvade.velocity.mult(30));
        //circle(enemyFuturePosition.x, enemyFuturePosition.y, 5);
        //print("IN CONTACT");
        //this.findClosestItem();
        //push the vehicle in direction such that its trajectory is pointing towards target
        //let directionTowardsEnemy = p5.Vector.sub(this.agentToEvade.position, this.position);

        
        //this.targetCoordinates.copy(enemyFuturePosition);
        
        
        //var trajectory = p5.Vector.sub(this.targetCoordinates, this.position);
        //var trajectory = directionTowardsEnemy;
        
        //trajectory.normalize(); //set's max speed to 1
        //this.targetVector = trajectory;
        //rotate(targetVector.heading());
        // if(this.flee())
        // 	var x = 1;
        // else
        

        //this.steeringForce = p5.Vector.sub(this.targetVector.mult(-1), this.velocity);
        //this.steeringForce.mult(100);

        //once they are no longer at risk of breaking social distance, reset flags

        //if that agent is closer to the item than this agent, then this agent needs to leave
        if (this.position.dist(this.closestItemLocation) >= this.agentToEvade.position.dist(this.closestItemLocation)) {
            //print(this.name + " AM LEAVING");
            this.iAmGoingToSwitch = true;
            this.switchTarget();
        }
        //else	
        //	this.switchTarget();

        
    }


    drawDirection() {
        var length = 30;
        this.point2 = p5.Vector.add(this.position, this.targetVector.mult(length));
        line(this.position.x, this.position.y, this.point2.x, this.point2.y);
    }


    //potential performance issues due to multiple translate calls per frame
    draw() {
        // this.position.x = this.position.x - (this.agentSize/2);
        push();
        translate(-width/2, -height/2);
        translate(this.position.x,this.position.y);
        push();
        push();
        //rotate(this.velocity.heading());
        //triangle(width/2, height/2, width/2-this.agentSize/2,height/2-this.agentSize,
            //width/2+this.agentSize/2,height/2-this.agentSize);
        fill(this.colour);
        push();
        translate(-this.agentSize/2, -this.agentSize/2);
        square(width/2, height/2, this.agentSize);
        pop();
        //push();
        noFill();
        circle(width/2, height/2, this.socialDistanceValue);
        //pop();
        pop();
        translate(width/2, height/2);

        this.cough();
        
        pop();
        pop();
        this.drawDirection();
    }

    cough() {
        //while the perlin noise output is > 0.9 the agent will cough
        if (this.infected) {
            //var t = this.coughSize;
            //translate(-t/2 + (this.agentSize/2), -t/2 + (this.agentSize/2));
            var r = this.coughSize;
            beginShape();
            noFill();
            /*when traversing the perlin grid in a square journey we must start and stop
            at the same points when moving from one loop to the next so that the loop
            is closed */ 
            var perlinTraversal = 1/sliderBounceFactor.value();
            if (noise(this.xoff) > 0.6) {
                this.coughingAnimation = true;
                
            }
            if(this.coughingAnimation && this.coughSize < sliderCoughSize.value()) {
                this.coughSize += this.coughTravelSpeed;
                this.coughed = true;
                this.checkSpread();
            }
            else {
                this.coughSize = 20;
                this.coughingAnimation = false;
            }
            var bounceFactor = 15;
            for(var j=0; j <= this.coughSize; j+=5) {
                this.xoff += perlinTraversal; 
                var yBounce = map(noise(this.xoff,this.yoff), 0, 1, -bounceFactor, bounceFactor);
                vertex(j,yBounce);
            }
            for(var j=0; j <= this.coughSize; j+=5) {
                this.yoff += perlinTraversal; 
                var xBounce = map(noise(this.xoff,this.yoff), 0, 1, -bounceFactor, bounceFactor);
                vertex(this.coughSize+xBounce,j);
            }
            for(var j=this.coughSize; j >= 0; j-=5) {
                this.xoff -= perlinTraversal; 
                var yBounce = map(noise(this.xoff,this.yoff), 0, 1, -bounceFactor, bounceFactor);
                vertex(j,this.coughSize+yBounce);
            }
            for(var j=this.coughSize; j >= 0; j-=5) {
                this.yoff -= perlinTraversal; 
                var xBounce = map(noise(this.xoff,this.yoff), 0, 1, -bounceFactor, bounceFactor);
                vertex(xBounce,j);
            }
            this.xoff += sliderCoughWobbleFactor.value();
            //print(noise(this.xoff));
            this.yoff += sliderCoughWobbleFactor.value(); 

            translate(-this.coughSize/2,-this.coughSize/2);


            endShape(CLOSE);

            // this.coughed = false;
        }
    }

    checkSpread() {
        if(this.infected && !this.tallied) {
            infectionTimings.push(getTime());
            this.tallied = true;
        }
        for (var i=0; i<agents.length; i++) {
            if (this != agents[i]) {
                if ((this.position.dist(agents[i].position) + this.agentSize/2 < this.coughSize)) {
                    print("YOU " + agents[i].name + "HAVE BEEN INFECTED????");
                    agents[i].infected = true;
                    //agentInfectionTimings.push(getTime());
                    if(!agents[i].tallied) {
                        infectionTimings.push(getTime());
                        agents[i].tallied = true;
                    }
                }
            }
        }
    }

    //is this hacky? will eventually want to move all the translates to this function
    move() {
        if (!this.finishedShopping) {
            //this.checkCaught();
            this.findClosestItem();

            this.checkPickedUpItem();


            if (this.checkBreakingSocialDistance() == true) {
                //print("breaking social distance");

                this.evade();
            }
            
            this.seek();
            this.steeringForce.limit(this.max_force);
            this.acceleration.add(this.steeringForce);
            this.velocity.add(this.acceleration);
            this.velocity.limit(this.max_speed);
            this.position.x += this.velocity.x;
            this.position.y += this.velocity.y;
            this.acceleration.x = 0;
            this.acceleration.y = 0;
            if(this.position.x > simWidth || this.position.y > simHeight
                || this.position.x < 0 || this.position.y < 0)
                this.velocity = p5.Vector.random2D();
            
            if(this.infected)
                this.coughed = false;
        
        }
    }

    leaveStore() {
        if (this.infected) {
            this.position.x = 999;
            this.position.y = 999;
        }
        else {
            this.position.x = -999;
            this.position.y = -999;
        }
        this.finishedShopping = true;
    }
    
}