class Item {
	constructor(name, colour) {
		this.name = name;
		this.colour = colour;
		this.position = createVector(random(0,simWidth), random(0,simHeight));
		
		//Once the item is picked up it will respawn in a different location
		this.pickedUp = false;

	}

	draw() {
		fill(this.colour);
		circle(this.position.x, this.position.y, 10);
	}

	respawn() {
		//if (this.pickedUp == true) {
			this.position.x = random(0,simWidth);
			this.position.y = random(0,simHeight);
			this.colour = random(colorNames);
			//this.pickedUp = false;
		//}
	}

	relocate() {
		//print("I HAVE BEEN PICKED UP");
	}
}